package ru.kpfu.itis.ui;

import java.io.File;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import ru.kpfu.itis.commands.CommandExecutor;
import ru.kpfu.itis.commands.exceptions.CommandExecutionException;
import ru.kpfu.itis.commands.exceptions.CommandExecutionWarning;
import ru.kpfu.itis.commands.exceptions.UnsupportedCommandException;
import ru.kpfu.itis.ui.views.MainMenuController;

/**
 * Created by Filippov on 02.03.2017.
 */
public class MenuUI extends Application implements IUI {
    private Stage stage;
    private CommandExecutor exec = new CommandExecutor(this);
    private File currentDir = new File("C:/");

    @Override
    public void open() {
        launch();
    }

    @Override
    public String sendNextCommand(String command) throws UnsupportedCommandException, CommandExecutionException, CommandExecutionWarning {
        return exec.execute(command);
    }

    @Override
    public void close() {
        stage.close();
    }

    @Override
    public File getCurrentDirectory() {
        return currentDir;
    }

    @Override
    public void setCurrentDirectory(File newDir) {
        currentDir = newDir;
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        this.stage = primaryStage;
        FXMLLoader loader = new FXMLLoader(
                MenuUI.class.getClassLoader().getResource("mainmenu.fxml")
        );
        AnchorPane rootLayout = loader.load();
        Scene scene = new Scene(rootLayout);
        primaryStage.setScene(scene);

        MainMenuController contr = loader.getController();
        contr.setStage(primaryStage);

        primaryStage.show();
    }
}