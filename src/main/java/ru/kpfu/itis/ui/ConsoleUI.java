package ru.kpfu.itis.ui;

import java.io.File;
import java.util.Scanner;
import ru.kpfu.itis.commands.CommandExecutor;
import ru.kpfu.itis.commands.exceptions.CommandExecutionException;
import ru.kpfu.itis.commands.exceptions.CommandExecutionWarning;
import ru.kpfu.itis.commands.exceptions.UnsupportedCommandException;

/**
 * Created by Filippov on 02.03.2017.
 */
public class ConsoleUI implements IUI {
    private CommandExecutor exec = new CommandExecutor(this);
    private File currentDir = new File("C:/");

    private Scanner scan = null;

    @Override
    public void open() {
        scan = new Scanner(System.in);

        String line = "";
        while (line.equals("exit") == false) {
            System.out.print(currentDir.getAbsolutePath() + "> ");
            line = scan.nextLine().trim();

            String result;
            try {
                result = sendNextCommand(line);
            } catch (UnsupportedCommandException | CommandExecutionException | CommandExecutionWarning e) {
                result = e.getMessage();
            }
            System.out.println(result);
        }
    }

    @Override
    public String sendNextCommand(String command) throws UnsupportedCommandException, CommandExecutionException, CommandExecutionWarning {
        return exec.execute(command);
    }

    @Override
    public void close() {
        if (scan != null) {
            scan.close();
        }
    }

    @Override
    public File getCurrentDirectory() {
        return currentDir;
    }

    @Override
    public void setCurrentDirectory(File newDir) {
        currentDir = newDir;
    }
}
