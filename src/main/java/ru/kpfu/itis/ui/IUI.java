package ru.kpfu.itis.ui;

import java.io.File;
import ru.kpfu.itis.commands.exceptions.CommandExecutionException;
import ru.kpfu.itis.commands.exceptions.CommandExecutionWarning;
import ru.kpfu.itis.commands.exceptions.UnsupportedCommandException;

/**
 * Created by Filippov on 02.03.2017.
 */
public interface IUI {
    void open();

    String sendNextCommand(String command) throws UnsupportedCommandException, CommandExecutionException, CommandExecutionWarning;

    void close();

    File getCurrentDirectory();

    void setCurrentDirectory(File newDir);
}
