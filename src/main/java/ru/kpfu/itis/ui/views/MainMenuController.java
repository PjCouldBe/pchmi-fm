package ru.kpfu.itis.ui.views;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextInputDialog;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Font;
import javafx.stage.DirectoryChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import ru.kpfu.itis.app.SharedData;
import ru.kpfu.itis.commands.Commands;
import ru.kpfu.itis.commands.exceptions.CommandExecutionException;
import ru.kpfu.itis.commands.exceptions.CommandExecutionWarning;
import ru.kpfu.itis.commands.exceptions.UnsupportedCommandException;
import ru.kpfu.itis.ui.IUI;
import ru.kpfu.itis.ui.MenuUI;

/**
 * Created by Filippov on 04.03.2017.
 */
public class MainMenuController {
    private Stage stage;
    private IUI menuui;

    public MainMenuController() {
        this.menuui = SharedData.getUI();
    }

    @FXML
    private Label pwdLabel;

    @FXML
    private ListView<String> fileList;

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @FXML
    public void initialize() {
        this.pwdLabel.setText("C:/");

        fileList.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        fileList.setCellFactory(l -> new ListCell<String>() {
            @Override
            protected void updateItem(String item, boolean empty) {
                super.updateItem(item, empty);

                if (empty || item == null) {
                    setText(null);
                } else {
                    setText(item);
                    setFont(Font.font(16));
                }
            }
        });
        menuui.setCurrentDirectory(new File("C:/"));
        ls();
    }

    private void alert(Alert.AlertType type, Exception e, String title) {
        Alert alert = new Alert(type);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(e.getMessage());
        alert.showAndWait();
    }
    private void confirm(String text) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Operation success!");
        alert.setHeaderText(null);
        alert.setContentText(text);
        alert.showAndWait();
    }

    private void ls() {
       /* try {
            menuui.sendNextCommand(Commands.CD.getCommandName() + " " + pwdLabel.getText());
        } catch (UnsupportedCommandException e) {
            alert(Alert.AlertType.ERROR, e, "Error!");
        } catch (CommandExecutionException e) {
            alert(Alert.AlertType.ERROR, e, "Dir change error!");
        } catch (CommandExecutionWarning e) {
            alert(Alert.AlertType.ERROR, e, "Warning!");
        }*/

        try {
            List<String> files = Arrays.asList( menuui.sendNextCommand(Commands.LIST.getCommandName()).split("\n") );
            fileList.setItems( FXCollections.observableList(files) );
        } catch (UnsupportedCommandException e) {
            alert(Alert.AlertType.ERROR, e, "Error!");
        } catch (CommandExecutionException e) {
            alert(Alert.AlertType.ERROR, e, "Getting files list error!");
        } catch (CommandExecutionWarning e) {
            alert(Alert.AlertType.ERROR, e, "Warning!");
        }

    }

    @FXML
    public void chooseFile() {
        DirectoryChooser dc = new DirectoryChooser();
        dc.setInitialDirectory( menuui.getCurrentDirectory() );

        final Stage dcStage = new Stage();
        dcStage.initOwner(stage);
        dcStage.initModality(Modality.WINDOW_MODAL);

        final File selectedDir = dc.showDialog(stage);
        if (selectedDir != null) {
            String path = selectedDir.getAbsolutePath();
            menuui.setCurrentDirectory(selectedDir);
            pwdLabel.setText(path);
            ls();
        }
    }

    @FXML
    public void newFile() {
        TextInputDialog dialog = new TextInputDialog("");
        dialog.setTitle("Create New File");
        dialog.setHeaderText(null);
        dialog.setContentText("Enter file name to create: ");

        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()) {
            try {
                String callback = menuui.sendNextCommand(Commands.CREATE.getCommandName() + " " + result.get());
                confirm(callback);
                ls();
            } catch (UnsupportedCommandException | CommandExecutionException e) {
                fileCreationAlert(Alert.AlertType.ERROR);
            } catch (CommandExecutionWarning e) {
                alert(Alert.AlertType.WARNING, e, "File creation Error!");
            }
        } else {
            fileCreationAlert(Alert.AlertType.WARNING);
        }
    }
    private void fileCreationAlert(Alert.AlertType type) {
        Alert alert = new Alert(type);
        alert.setTitle("File creation Error!");
        alert.setHeaderText(null);
        alert.setContentText("No name was specified for new file.\n Nothing was created!");
        alert.showAndWait();
    }

    @FXML
    public void help() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MenuUI.class.getClassLoader().getResource("helpmenu.fxml"));
            AnchorPane pane = loader.load();

            Stage helpStage = new Stage();
            helpStage.setTitle("Help");
            helpStage.initModality(Modality.WINDOW_MODAL);
            helpStage.initOwner(stage);
            Scene scene = new Scene(pane);

            HelpMenuController contr = loader.getController();
            contr.setStage(helpStage);
            helpStage.setScene(scene);

            helpStage.showAndWait();
        } catch (IOException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error opening help!");
            alert.setHeaderText(null);
            alert.setContentText("Some internal error occurred while opening help window!");
            alert.showAndWait();
        }
    }

    @FXML
    public void renameFiles() {
        try {
            SharedData.setFilesToRename( fileList.getSelectionModel().getSelectedItems() );

            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MenuUI.class.getClassLoader().getResource("renamemenu.fxml"));
            AnchorPane pane = loader.load();

            Stage renameStage = new Stage();
            renameStage.setTitle("Rename files");
            renameStage.initModality(Modality.WINDOW_MODAL);
            renameStage.initOwner(stage);

            Scene scene = new Scene(pane);
            renameStage.setScene(scene);

            RenameMenuController contr = loader.getController();
            contr.setStage(renameStage);

            renameStage.showAndWait();
            ls();
        } catch (IOException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error opening rename managing window!");
            alert.setHeaderText(null);
            alert.setContentText(e.getCause() instanceof CommandExecutionException
                    ? e.getCause().getMessage()
                    : "Some internal error occurred while opening rename managing window!");
            alert.showAndWait();
        }
    }
}
