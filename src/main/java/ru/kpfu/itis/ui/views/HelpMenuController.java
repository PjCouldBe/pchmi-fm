package ru.kpfu.itis.ui.views;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ScrollPane;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import ru.kpfu.itis.commands.Commands;
import ru.kpfu.itis.commands.exceptions.UnsupportedCommandException;
import ru.kpfu.itis.help.HelpCenter;

/**
 * Created by Filippov on 04.03.2017.
 */
public class HelpMenuController {
    @FXML
    private ComboBox<String> dropDownList;
    @FXML
    private ScrollPane scrollPane;
    private Stage stage;

    @FXML
    public void initialize() {
        List<String> list = Arrays.stream(Commands.values()).map(Commands::getCommandName).collect(Collectors.toList());
        list.add("");
        list.sort(Comparator.comparingInt(String::length));

        this.dropDownList.setItems( FXCollections.observableList(list) );
    }

    @FXML
    public void switchActiveCommand() {
        Commands c = null;
        String text;
        try {
            String s = dropDownList.getSelectionModel().getSelectedItem();
            text = s.isEmpty() ? HelpCenter.getGeneralReference()
                               : HelpCenter.getCommandUsageReference( Commands.valueOfCommand(s) );

        } catch (UnsupportedCommandException e) {
            text = "";
        }

        Text txt = new Text(text);
        txt.setFont(Font.font(16));
        txt.wrappingWidthProperty().bind(stage.widthProperty());
        scrollPane.setFitToWidth(true);
        scrollPane.setContent(txt);
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @FXML
    public void closeHelp() {
        stage.close();
    }
}
