package ru.kpfu.itis.ui.views;

import java.util.List;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.stage.Stage;
import ru.kpfu.itis.app.SharedData;
import ru.kpfu.itis.commands.Commands;
import ru.kpfu.itis.commands.exceptions.CommandExecutionException;
import ru.kpfu.itis.commands.exceptions.CommandExecutionWarning;
import ru.kpfu.itis.commands.exceptions.UnsupportedCommandException;
import ru.kpfu.itis.ui.IUI;

/**
 * Created by Filippov on 04.03.2017.
 */
public class RenameMenuController {
    private Stage stage;
    private IUI menuui;
    private List<String> filesToRename;

    private String[] newPaths;

    public RenameMenuController() throws CommandExecutionException {
        this.menuui = SharedData.getUI();

        if (SharedData.getFilesToRename() == null || SharedData.getFilesToRename().isEmpty())  {
            throw new CommandExecutionException("Nothing is selected! Cannot run the command!");
        }
        this.filesToRename = SharedData.getFilesToRename();
        this.newPaths = new String[this.filesToRename.size()];
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @FXML
    private TableView<String> filesToRenameTable;
    @FXML
    private TableColumn<String, String> labelColumn;
    @FXML
    private TableColumn<String, String> inputFieldsColumn;

    @FXML
    public void initialize() {
        filesToRenameTable.setEditable(true);

        labelColumn.setCellValueFactory( cellData -> new SimpleStringProperty(cellData.getValue()) );
        labelColumn.setEditable(false);
        inputFieldsColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        inputFieldsColumn.setOnEditCommit(t -> newPaths[t.getTablePosition().getRow()] = t.getNewValue());

        filesToRenameTable.setItems( FXCollections.observableList(filesToRename) );
    }


    public void onOK() throws UnsupportedCommandException, CommandExecutionWarning, CommandExecutionException {
        StringBuilder sb = new StringBuilder(Commands.RENAME.getCommandName())
                .append(" -source ");

        sb.append(String.join(" ", filesToRename));
        sb.append(" -target ");

        for (int i = 0; i < newPaths.length; i++) {
            String item = newPaths[i];
            if (item == null || item.isEmpty()) {
                Alert errorAlert = new Alert(Alert.AlertType.ERROR);
                errorAlert.setTitle("Rename Error!");
                errorAlert.setHeaderText(null);
                errorAlert.setContentText("The target name for " + filesToRename.get(i) + " file was not set! " +
                        "Cannot rename until you specify target names for all files!");
                errorAlert.showAndWait();
                return;
            }
        }

        sb.append( String.join(" ", newPaths) );
        try {
            String res = menuui.sendNextCommand(sb.toString());
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Operation success!");
            alert.setHeaderText(null);
            alert.setContentText(res);
            alert.showAndWait();
        } catch (UnsupportedCommandException | CommandExecutionException e) {
            alert(Alert.AlertType.ERROR, e, "File renaming Error!");
        } catch (CommandExecutionWarning e) {
            alert(Alert.AlertType.WARNING, e, "File renaming Error!");
        }

        stage.close();
    }
    private void alert(Alert.AlertType type, Exception e, String title) {
        Alert alert = new Alert(type);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(e.getMessage());
        alert.showAndWait();
    }

    public void onCancel() {
        stage.close();
    }
}
