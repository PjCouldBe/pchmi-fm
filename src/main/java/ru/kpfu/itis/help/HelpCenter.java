package ru.kpfu.itis.help;

import static ru.kpfu.itis.commands.Commands.*;

import com.google.common.collect.ImmutableBiMap;
import com.google.common.collect.ImmutableMap;
import ru.kpfu.itis.commands.Commands;

/**
 * Created by Filippov on 02.03.2017.
 */
public class HelpCenter {
    public static final String USAGE = "--USAGE--";
    private static final String GENERAL_REFERENCE;

    private static final ImmutableMap<Commands, String> CMD_USAGE_LIST;

    static {
        GENERAL_REFERENCE = "The list of available commands:\n" +
                "? - writes help instructions for specified command or for all of them shortly.\n" +
                "list - shows list of files and folders of current directory.\n" +
                "dir - shows the current directory path.\n" +
                "cdir - changes the current directory to the specified folder.\n" +
                "new - creates new file with specified name.\n" +
                "rename - renames the group of files using specified names.\n" +
                "exit - stops the application.";

        final String HELP_USAGE = "? [cmd]\n" +
                "Writes help instructions for all commands shortly (if cmd argument is empty) " +
                "or for specified command in details (otherwise).\n" +
                "Has no keys. Other arguments or key like input (started with '-' or '/') will be ignored.";
        final String LIST_USAGE = "Shows list of files and folders of current directory.\n" +
                "No arguments or keys (any extra input will be ignored).";
        final String DIR_USAGE = "Shows the current directory path.\n" +
                "No arguments or keys (any extra input will be ignored).";
        final String CDIR_USAGE = "cdir [/D] <path>\n" +
                "Changes the current directory to the folder specified by <path> parameter. " +
                "The path has to be absolute, so you need to write down full path to desired folder. " +
                "If the path contains spaces it should be wrapped in quotes (\"<path>\").\n" +
                "Keys:\n" +
                "/D - is used to switch not only folder but current disk too. You can't switch e. g. from C:/ to D:/ " +
                "without this key. The key is to be pointed before path argument.\n" +
                "Other arguments or keys (input started with '-' or '/') will be ignored.";
        final String NEW_USAGE = "new <filename>\n" +
                "Creates new file with name specified by <filename> argument. " +
                "Filename cannot be empty. If it contains spaces it should be wrapped in quotes (\"<filename>\").\n" +
                "Has no keys. Other arguments or key like input (started with '-' or '/') will be ignored.";
        final String RENAME_USAGE = "rename -source <list-of-source-filenames> -target <list-of-new-names>\n" +
                "Renames the group of files (with source names specified under the -source key) using names" +
                "specified under -target key. List of names under both keys are pointed with space delimiter. " +
                "If some paths contain spaces every of them should be wrapped in quotes (\"<name>\"). " +
                "The source names list length has to be equal to target one length.\n" +
                "Keys:\n" +
                "-source - is used to specify names of files in current directory to rename.\n" +
                "-target - is used to specify new names of files specified under -source key. " +
                "Target name matches the source name relatively.\n" +
                "Other arguments (that are not violate the above described syntax) " +
                "or keys (input started with '-' or '/') will be ignored.";
        final String EXIT_USAGE = "Stops and closes the application.\n" +
                "No arguments or keys (any extra input will be ignored).";

        CMD_USAGE_LIST = ImmutableBiMap.<Commands, String>builder()
                .put(HELP, HELP_USAGE)
                .put(LIST, LIST_USAGE)
                .put(PWD, DIR_USAGE)
                .put(CD, CDIR_USAGE)
                .put(CREATE, NEW_USAGE)
                .put(RENAME, RENAME_USAGE)
                .put(EXIT, EXIT_USAGE)
                .build();
    }

    public static String getGeneralReference() {
        return GENERAL_REFERENCE;
    }

    public static String getCommandUsageReference(Commands cmd) {
        return CMD_USAGE_LIST.get(cmd);
    }
}
