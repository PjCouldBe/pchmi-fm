package ru.kpfu.itis.commands;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import ru.kpfu.itis.commands.exceptions.UnsupportedCommandException;

/**
 * Created by Filippov on 05.03.2017.
 */
public class CommandParser {
    public CompositeCommand parseCommand(String cmd) throws UnsupportedCommandException {
        int ind = cmd.indexOf(' ');
        Commands userC = Commands.valueOfCommand( ind == -1 ? cmd : cmd.substring(0, ind) );

        String[] rest = ind == -1 ? new String[] {} : splitKeysAndArguments( cmd.substring(ind+1) );

        int keyCount = 0;
        for (String aRest : rest) {
            if (aRest.charAt(0) == '-' || aRest.charAt(0) == '/') {
                keyCount++;
            }
        }

        String[] keys = new String[keyCount];
        String[] args;
        if (userC == Commands.RENAME) {
            args = new String[rest.length - keyCount + 1];
            String lastKey = "";
            int k1 = 0, k2 = 0;
            for (String s : rest) {
                if (s.charAt(0) == '-' || s.charAt(0) == '/') {
                    keys[k1++] = s;
                    lastKey = s;
                    if (s.equals("-target")) k2++;
                } else {
                    if (lastKey.equals("-source") || lastKey.equals("-target")) {
                        args[k2++] = s;
                    }
                }
            }

            //trim to size
            String[] argsN = new String[k2];
            System.arraycopy(args, 0, argsN, 0, k2);
            args = argsN;
        } else {
            args = new String[rest.length - keyCount];
            int k1 = 0, k2 = 0;
            for (String s : rest) {
                if (s.charAt(0) == '-' || s.charAt(0) == '/') {
                    keys[k1++] = s;
                } else {
                    args[k2++] = s;
                }
            }
        }

        return new CompositeCommand(userC, keys, args);
    }

    private String[] splitKeysAndArguments(String subjectString) {
        List<String> matchList = new ArrayList<>();
        Pattern regex = Pattern.compile("[^\\s\"']+|\"([^\"]*)\"|'([^']*)'");
        Matcher regexMatcher = regex.matcher(subjectString);
        while (regexMatcher.find()) {
            if (regexMatcher.group(1) != null) {
                // Add double-quoted string without the quotes
                matchList.add(regexMatcher.group(1));
            } else if (regexMatcher.group(2) != null) {
                // Add single-quoted string without the quotes
                matchList.add(regexMatcher.group(2));
            } else {
                // Add unquoted word
                matchList.add(regexMatcher.group());
            }
        }

        return matchList.toArray(new String[matchList.size()]);
    }
}
