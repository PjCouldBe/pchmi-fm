package ru.kpfu.itis.commands.exceptions;

/**
 * Created by Filippov on 05.03.2017.
 */
public class CommandExecutionWarning extends Exception {
    public CommandExecutionWarning(String message) {
        super(message);
    }

    public CommandExecutionWarning(String message, Throwable cause) {
        super(cause.getMessage() + "\n" + message, cause);
    }
}
