package ru.kpfu.itis.commands.exceptions;

/**
 * Created by Filippov on 03.03.2017.
 */
public class UnsupportedCommandException extends Exception {
    public UnsupportedCommandException() {
        super(" Such command doesn't exists! ");
    }

    public UnsupportedCommandException(CharSequence msg) {
        super(" Such command doesn't exists! \n" + msg.toString());
    }
}
