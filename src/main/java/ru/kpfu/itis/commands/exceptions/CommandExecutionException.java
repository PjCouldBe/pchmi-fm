package ru.kpfu.itis.commands.exceptions;

/**
 * Created by Filippov on 05.03.2017.
 */
public class CommandExecutionException extends Exception {
    public CommandExecutionException(String message) {
        super(message);
    }

    public CommandExecutionException(String message, Throwable cause) {
        super(cause.getMessage() + "\n" + message, cause);
    }
}
