package ru.kpfu.itis.commands;

import ru.kpfu.itis.commands.exceptions.UnsupportedCommandException;

/**
 * Created by Filippov on 03.03.2017.
 */
public enum Commands {
    HELP("?"),
    LIST("list"),
    PWD("dir"),
    CD("cdir"),
    CREATE("new"),
    RENAME("rename"),
    EXIT("exit");

    Commands(String cmdName) {
        this.cmdName = cmdName;
    }

    private String cmdName;

    public String getCommandName() {
        return cmdName;
    }

    public static Commands valueOfCommand(String cmd) throws UnsupportedCommandException {
        switch (cmd) {
            case "?":
                return HELP;
            case "list":
                return LIST;
            case "dir":
                return PWD;
            case "cdir":
                return CD;
            case "new":
                return CREATE;
            case "rename":
                return RENAME;
            case "exit":
                return EXIT;
            default:
                throw new UnsupportedCommandException();
        }
    }
}
