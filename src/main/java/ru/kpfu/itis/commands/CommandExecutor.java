package ru.kpfu.itis.commands;

import java.io.File;
import java.util.Arrays;
import java.util.stream.Collectors;
import javafx.scene.control.Alert;
import ru.kpfu.itis.commands.exceptions.CommandExecutionException;
import ru.kpfu.itis.commands.exceptions.CommandExecutionWarning;
import ru.kpfu.itis.commands.exceptions.UnsupportedCommandException;
import ru.kpfu.itis.commands.handlers.CDirHandler;
import ru.kpfu.itis.commands.handlers.HelpHandler;
import ru.kpfu.itis.commands.handlers.ICommandHandler;
import ru.kpfu.itis.commands.handlers.NewHandler;
import ru.kpfu.itis.commands.handlers.RenameHandler;
import ru.kpfu.itis.help.HelpCenter;
import ru.kpfu.itis.ui.IUI;

/**
 * Created by Filippov on 02.03.2017.
 */
public class CommandExecutor {
    private IUI ui;
    private final CommandParser PARSER = new CommandParser();

    public CommandExecutor(IUI ui) {
        if (ui == null) {
            throw new NullPointerException();
        };
        this.ui = ui;
    }
    public void errorAlert() {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Application launch Error!");
        alert.setHeaderText(null);
        alert.setContentText("Error launching application! No UI instance was found!");
        alert.show();
    }

    public String execute(String cmd) throws CommandExecutionException, UnsupportedCommandException, CommandExecutionWarning {
        try {
            return execute( PARSER.parseCommand(cmd));
        } catch (UnsupportedCommandException e) {
            throw new UnsupportedCommandException(
                    HelpCenter.USAGE + "\n" +
                    HelpCenter.getGeneralReference());
        }
    }

    public String execute(CompositeCommand cc) throws CommandExecutionException, CommandExecutionWarning {
        Commands userCommand = cc.getUserCmd();
        String[] keys = cc.getKeys();
        String[] args = cc.getArgs();

        ICommandHandler handler;
        switch (userCommand) {
            case HELP:
                handler = new HelpHandler();
                break;
            case LIST:
                return Arrays.stream( ui.getCurrentDirectory().listFiles() ).map(File::getName).collect(Collectors.joining("\n"));
            case PWD:
                return ui.getCurrentDirectory().getAbsolutePath();
            case CD:
                handler = new CDirHandler(ui);
                break;
            case CREATE:
                handler = new NewHandler(ui);
                break;
            case RENAME:
                handler = new RenameHandler(ui);
                break;
            case EXIT:
                ui.close();
                System.exit(0);
                return null;
            default:
                throw new CommandExecutionException("");
        }

        return handler.handleCommand(keys, args);
    }
}
