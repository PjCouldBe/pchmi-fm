package ru.kpfu.itis.commands;

/**
 * Created by Filippov on 03.03.2017.
 */
public class CompositeCommand {
    private Commands userCmd;
    private String[] keys;
    private String[] args;

    public CompositeCommand(Commands userCmd, String[] keys, String[] args) {
        this.userCmd = userCmd;
        this.keys = keys;
        this.args = args;
    }

    public Commands getUserCmd() {
        return userCmd;
    }

    public String[] getKeys() {
        return keys;
    }

    public String[] getArgs() {
        return args;
    }
}
