package ru.kpfu.itis.commands.handlers;

import java.io.File;
import java.util.ArrayList;
import ru.kpfu.itis.commands.Commands;
import ru.kpfu.itis.commands.exceptions.CommandExecutionException;
import ru.kpfu.itis.commands.exceptions.CommandExecutionWarning;
import ru.kpfu.itis.help.HelpCenter;
import ru.kpfu.itis.ui.IUI;

/**
 * Created by Filippov on 07.03.2017.
 */
public class RenameHandler implements ICommandHandler {
    private IUI ui;

    public RenameHandler(IUI ui) {
        this.ui = ui;
    }

    @Override
    public String handleCommand(String[] keys, String[] args) throws CommandExecutionException, CommandExecutionWarning {
        int half = args.length / 2;
        if (args.length == 0) {
            throw new CommandExecutionException("Command arguments are missing!\n"
                    + HelpCenter.USAGE + "\n"
                    + HelpCenter.getCommandUsageReference(Commands.RENAME));
        } else if (args.length % 2 == 0 || args[half] != null) {
            throw new CommandExecutionException("Source files to rename amount does not equal to target names amount!\n"
                    + HelpCenter.USAGE + "\n"
                    + HelpCenter.getCommandUsageReference(Commands.RENAME));
        }

        ArrayList<String> unexisting = new ArrayList<>();
        ArrayList<String> wrong = new ArrayList<>();
        int renamed = 0;
        for (int i = 0; i < half; i++) {
            File sfile = new File(ui.getCurrentDirectory() + File.separator + args[i]);
            File tFile = new File(ui.getCurrentDirectory() + File.separator + args[half + 1 + i]);

            if (!sfile.exists()) {
                unexisting.add(sfile.getName());
            } else if (tFile.exists()) {
                wrong.add(tFile.getName());
            } else {
                sfile.renameTo(tFile);
                renamed++;
            }
        }

        String successMsg = renamed + (renamed == 1 ? " file has" : " files have") + " been successfully renamed!";
        if (unexisting.size() == 0 && wrong.size() == 0) {
            return successMsg;
        } else {
            StringBuilder error = new StringBuilder();

            if (unexisting.size() != 0) {
                boolean isAlone = unexisting.size() == 1;
                error.append("File").append(isAlone ? "" : "s")
                        .append(" ").append(String.join(", ", unexisting))
                        .append(" ").append(isAlone ? "doesn't" : "don't")
                        .append(" exist. Can't rename nonexistent files!")
                        .append("\n");
            }

            if (wrong.size() != 0) {
                boolean isAlone = wrong.size() == 1;
                error.append("File").append(isAlone ? "" : "s")
                        .append(" ").append(String.join(", ", wrong))
                        .append(" already exist").append(isAlone ? "s" : "")
                        .append(". Can't rename if files with specified names already exist!")
                        .append("\n");
            }

            error.append(successMsg);
            throw new CommandExecutionWarning(error.toString());
        }
    }
}
