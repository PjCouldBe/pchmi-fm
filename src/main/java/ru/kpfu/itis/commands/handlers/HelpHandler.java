package ru.kpfu.itis.commands.handlers;

import ru.kpfu.itis.commands.Commands;
import ru.kpfu.itis.commands.exceptions.CommandExecutionException;
import ru.kpfu.itis.commands.exceptions.CommandExecutionWarning;
import ru.kpfu.itis.commands.exceptions.UnsupportedCommandException;
import ru.kpfu.itis.help.HelpCenter;

/**
 * Created by Filippov on 07.03.2017.
 */
public class HelpHandler implements ICommandHandler {
    @Override
    public String handleCommand(String[] keys, String[] args) throws CommandExecutionException, CommandExecutionWarning {
        String res;

        if (args.length > 0) {
            try {
                Commands argC = Commands.valueOfCommand(args[0]);
                res = HelpCenter.getCommandUsageReference(argC);
            } catch (UnsupportedCommandException e) {
                throw new CommandExecutionException("There is no command you'd like to get help about!\n\n" +
                        HelpCenter.getGeneralReference());
            }
        } else {
            res = HelpCenter.getGeneralReference();
        }

        return res + "\n";
    }
}
