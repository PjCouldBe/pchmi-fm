package ru.kpfu.itis.commands.handlers;

import java.io.File;
import java.io.IOException;
import ru.kpfu.itis.commands.exceptions.CommandExecutionException;
import ru.kpfu.itis.commands.exceptions.CommandExecutionWarning;
import ru.kpfu.itis.ui.IUI;

/**
 * Created by Filippov on 07.03.2017.
 */
public class NewHandler implements ICommandHandler {
    private IUI ui;

    public NewHandler(IUI ui) {
        this.ui = ui;
    }

    @Override
    public String handleCommand(String[] keys, String[] args) throws CommandExecutionException, CommandExecutionWarning {
        if (args.length == 0) {
            throw new CommandExecutionWarning("No name was specified for new file.\nNothing was created!");
        } else {
            File f = new File(ui.getCurrentDirectory().getAbsolutePath() + File.separator + args[0]);
            if (f.exists()) {
                throw new CommandExecutionWarning("File with specified name already exists!\nNothing was created!");
            }

            try {
                f.createNewFile();
            } catch (IOException e) {
                throw new CommandExecutionException("Couldn't create file with specified name!", e);
            }
        }

        return "File successfully created!";
    }
}
