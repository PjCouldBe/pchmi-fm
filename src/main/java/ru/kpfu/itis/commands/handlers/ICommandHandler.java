package ru.kpfu.itis.commands.handlers;

import ru.kpfu.itis.commands.exceptions.CommandExecutionException;
import ru.kpfu.itis.commands.exceptions.CommandExecutionWarning;

/**
 * Created by Filippov on 07.03.2017.
 */
public interface ICommandHandler {
    String handleCommand(String[] keys, String[] args) throws CommandExecutionException, CommandExecutionWarning;
}
