package ru.kpfu.itis.commands.handlers;

import java.io.File;
import ru.kpfu.itis.commands.Commands;
import ru.kpfu.itis.commands.exceptions.CommandExecutionException;
import ru.kpfu.itis.commands.exceptions.CommandExecutionWarning;
import ru.kpfu.itis.help.HelpCenter;
import ru.kpfu.itis.ui.IUI;

/**
 * Created by Filippov on 07.03.2017.
 */
public class CDirHandler implements ICommandHandler {
    private IUI ui;

    public CDirHandler(IUI ui) {
        this.ui = ui;
    }

    @Override
    public String handleCommand(String[] keys, String[] args) throws CommandExecutionException, CommandExecutionWarning {
        if (args.length == 0) {
            throw new CommandExecutionException("There is no path of new folder given to switch to! Nothing changes.");
        }

        File newDir = new File( args[0] );
        if ( ! newDir.exists() ) {
            throw new CommandExecutionException("The folder with specified name does not exist! Nothing changes.");
        }
        if ( ! newDir.isDirectory()) {
            throw new CommandExecutionWarning("The specified path belongs to file, not to a folder! Nothing changes.");
        }

        if (keys.length >= 1 && keys[0].equals("/D")) {    //if key '/D' was pointed!
            ui.setCurrentDirectory(newDir);
            return "Directory was changed successfully!";
        } else {
            String oldDirPath = ui.getCurrentDirectory().getAbsolutePath();
            String newDirPath = args[0];

            if (oldDirPath.charAt(0) == newDirPath.charAt(0)) {
                ui.setCurrentDirectory(newDir);
                return "Directory was changed successfully!";
            } else {
                throw new CommandExecutionWarning("Cannot switch disk without '/D' key flag! Nothing changes.\n"
                        + HelpCenter.USAGE + "\n"
                        + HelpCenter.getCommandUsageReference(Commands.CD));
            }
        }
    }
}
