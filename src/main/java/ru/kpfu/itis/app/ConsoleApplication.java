package ru.kpfu.itis.app;

import java.io.IOException;
import ru.kpfu.itis.ui.ConsoleUI;

/**
 * Created by Filippov on 02.03.2017.
 */
public class ConsoleApplication {
    public static void main(String[] args) throws IOException {
        new ConsoleUI().open();
    }
}
