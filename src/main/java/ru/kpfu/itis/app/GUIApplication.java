package ru.kpfu.itis.app;

import ru.kpfu.itis.ui.MenuUI;

/**
 * Created by Filippov on 09.03.2017.
 */
public class GUIApplication {
    public static void main(String[] args) {
        SharedData.initialize(new MenuUI());
        SharedData.getUI().open();
    }
}
