package ru.kpfu.itis.app;

import java.util.List;
import ru.kpfu.itis.ui.IUI;

/**
 * Created by Filippov on 09.03.2017.
 */
public class SharedData {
    private static SharedData INSTANCE;

    private final IUI UI;
    private List<String> filesToRename;

    public static void initialize(IUI ui) {
        INSTANCE = new SharedData(ui);
    }

    private SharedData(IUI ui) {
        this.UI = ui;
    }

    public static IUI getUI() {
        return INSTANCE.UI;
    }


    public static List<String> getFilesToRename() {
        return INSTANCE.filesToRename;
    }

    public static void setFilesToRename(List<String> filesToRename) {
        INSTANCE.filesToRename = filesToRename;
    }
}
